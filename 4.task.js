function doubleFactorial(n) {
	if(n >= 2) {
  	return n * doubleFactorial((n-2));
  }  
  else {
  	return 1;
  }  
}

console.log('doubleFactorial(14) = ' + doubleFactorial(14));
console.log('doubleFactorial(9) = ' + doubleFactorial(9));