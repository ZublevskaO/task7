const someVars = function (x, y) {
    if (x < y) {
        return x + y
    }
    else if (x > y) {
        return x - y
    }
    else {
        return x * y
    }
}

result = someVars(4, 4)
console.log(result)