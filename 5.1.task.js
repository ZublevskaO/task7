let person1 = 
    {name: "Jane", age: 18};
let person2 =
    {name: "Anna", age: 33};

function compareAge(person1, person2) {
    if (person1.age < person2.age) {
    return console.log([person1.name] + ' is younger than ' + [person2.name])
    }
    else if (person1.age > person2.age) {
    return console.log([person1.name] + ' is older than ' + [person2.name])
    }
    else {
    return console.log([person1.name] + ' is the same age as ' + [person2.name])
    }
}

compareAge(person1, person2);