const person = [
    {name: "Jane", age: 18},
    {name: "Anna", age: 33},
    {name: "Leo", age: 25},
    {name: "Tom", age: 12}
]
person.sort(compareAge)
function compareAge(a, b) {
    if(a.age > b.age)
    return 1
    if(a.age < b.age)
    return -1
    return 0
}
console.log(person)